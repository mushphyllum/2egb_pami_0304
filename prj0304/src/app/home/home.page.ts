import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "App Motos";

  cards = [
    {
      titulo: "Moto 1",
      subtitulo: "Primeira moto",
      conteudo: "Este é um exemplo de como ficarão os cards do meu app",
      foto: "https://www.motonline.com.br/noticia/wp-content/uploads/2021/07/ducati-superleggera-moto-mais-cara-2-800x420.jpg"
    },
    {
      titulo: "Moto 2",
      subtitulo: "Segunda moto",
      conteudo: "Este é o segundo exemplo de card no meu aplicativo",
      foto: "https://garagem360.com.br/wp-content/uploads/2021/07/2020-BMW-K1600-GT-Side-View.jpg"
    },
    {
      titulo: "Moto 3",
      subtitulo: "Terceira moto",
      conteudo: "Este é o terceiro exemplo de card no meu aplicativo",
      foto: "https://i0.statig.com.br/bancodeimagens/6p/1l/w8/6p1lw8y1rg2ys92z21xvyp83k.jpg"
    },
    {
      titulo: "Moto 4",
      subtitulo: "Quarta moto",
      conteudo: "Exemplo 4",
      foto: "https://www.motoo.com.br/fotos/2021/10/960_720/yamaha_mt-03_2022_1_13102021_41880_960_720.jpg"
    }
  ];

  constructor() {}

}
